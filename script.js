let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friend:{
		hoenn:['May','Max'],
		kanto:['Brock','Misty'],

	},
	talk: function(){
		console.log('Pikachu! I choose you!');
	},
	
}
console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of square bracket notation: ');
console.log(trainer['pokemon']);
console.log('Result of talk method:');
trainer.talk();


let myPokemon1 = {
	name: 'Pikachu',
	level: 12,
	health: 24,
	attack: 12,
	tackle: function(){
		console.log('This pokemon tackled targetPokemon');
		console.log('targetPokemon health is now reduced to _targetPokemonHealth_');
	},
	faint: function(){
		console.log('pokemon fainted');
	}
}

console.log(myPokemon1);

let myPokemon2 = {
	name: 'Geodude',
	level: 8,
	health: 16,
	attack: 8,
	tackle: function(){
		console.log('This pokemon tackled targetPokemon');
		console.log('targetPokemon health is now reduced to _targetPokemonHealth_');
	},
	faint: function(){
		console.log('pokemon fainted');
	}
}

console.log(myPokemon2);

let myPokemon3 = {
	name: 'Mewtwo',
	level: 100,
	health: 200,
	attack: 100,
	tackle: function(){
		console.log('This pokemon tackled targetPokemon');
		console.log('targetPokemon health is now reduced to _targetPokemonHealth_');
	},
	faint: function(){
		console.log('pokemon fainted');
	}
}

console.log(myPokemon3);

function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		let targetPokemonHealth  = target.health - this.attack ;
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + targetPokemonHealth);

	this.faint = function(target){
		if(targetPokemonHealth <= 0){
			console.log(target.name + ' fainted');
		} 
		else {

		}
	}
		myPokemon1.health = targetPokemonHealth ;
		myPokemon2.health = targetPokemonHealth ;


	
	}
}


let pikachu = new Pokemon('Pikachu',12);
let geodude = new Pokemon('Geodude',8);
let mewtwo =  new Pokemon('Mewtwo',100)

geodude.tackle(pikachu);
console.log(myPokemon1);
mewtwo.tackle(geodude);
mewtwo.faint(geodude);
console.log(myPokemon2);
